package finder.repository.github.graphics;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import finder.repository.github.bean.RepositoryBean;
import finder.repository.github.githubfinder.R;

/**
 * Created by Carlos on 02/11/2016.
 */
public class RepositoryAdapter extends BaseAdapter {
    Activity activity;
    List<RepositoryBean> repositories;
    private String[] ids;
    private String[] names;
    private String[] descriptions;
    private String[] languages;
    private String[] issues;
    private String[] contributors;

    public RepositoryAdapter(Activity activity,List<RepositoryBean> repositories){
        super();
        this.activity = activity;
        this.repositories = repositories;
    }

    @Override
    public int getCount() {
        return repositories.size();
    }

    @Override
    public Object getItem(int position) {
        return repositories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class Holder{
        TextView repositoryName;
        TextView repositoryDescription;
        TextView repositoryLanguage;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        View view = convertView;
        if(view == null){
            holder = new Holder();
            LayoutInflater inflater = LayoutInflater.from(activity.getApplicationContext());
            view = inflater.inflate(R.layout.item_adapter,parent,false);
            holder.repositoryName = (TextView) view.findViewById(R.id.repositoryName);
            holder.repositoryDescription = (TextView) view.findViewById(R.id.repositoryDescription);
            holder.repositoryLanguage = (TextView) view.findViewById(R.id.repositoryLanguage);
            view.setTag(holder);
        }
        else{
            holder = (Holder) view.getTag();
        }
        holder.repositoryName.setText(repositories.get(position).getName());
        holder.repositoryDescription.setText(repositories.get(position).getDescription());
        holder.repositoryLanguage.setText(repositories.get(position).getIssues_url());
        return view;
    }

    public List<RepositoryBean> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<RepositoryBean> repositories) {
        this.repositories = repositories;
    }

    public String[] getIds() {
        ids = new String[repositories.size()];
        for(int i = 0; i < repositories.size(); i++){
            ids[i] = repositories.get(i).getId();
        }
        return ids;
    }

    public String[] getNames() {
        names = new String[repositories.size()];
        for(int i = 0; i < repositories.size(); i++){
            names[i] = repositories.get(i).getName();
        }
        return names;
    }

    public String[] getDescriptions() {
        descriptions = new String[repositories.size()];
        for(int i = 0; i < repositories.size(); i++){
            descriptions[i] = repositories.get(i).getDescription();
        }
        return descriptions;
    }

    public String[] getLanguages() {
        languages = new String[repositories.size()];
        for(int i = 0; i < repositories.size(); i++){
            languages[i] = repositories.get(i).getLanguage_link();
        }
        return languages;
    }

    public String[] getIssues() {
        issues = new String[repositories.size()];
        for(int i = 0; i < repositories.size(); i++){
            issues[i] = repositories.get(i).getIssues_url();
        }
        return issues;
    }

    public String[] getContributors() {
        contributors = new String[repositories.size()];
        for(int i = 0; i < repositories.size(); i++){
            contributors[i] = repositories.get(i).getContributors_link();
        }
        return contributors;
    }
}
