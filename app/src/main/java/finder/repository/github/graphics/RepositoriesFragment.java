package finder.repository.github.graphics;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.LinkedList;
import java.util.List;
import finder.repository.github.bean.RepositoryBean;
import finder.repository.github.dowloadRepos.DownloadManager;
import finder.repository.github.dowloadRepos.EndlessScroll;
import finder.repository.github.githubfinder.R;
import finder.repository.github.githubfinder.RepositoryActivity;

/**
 * Created by Carlos on 02/11/2016.
 */
public class RepositoriesFragment extends Fragment {
    private ProgressBar loading;
    private ListView repositoriesList;
    private TextView message;
    private RepositoryAdapter repositoryAdapter;
    private List<RepositoryBean> repositories;
    private DownloadManager downloadManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(savedInstanceState != null){
            String[] ids = savedInstanceState.getStringArray("ids");
            String[] names = savedInstanceState.getStringArray("names");
            String[] issues = savedInstanceState.getStringArray("issues");
            String[] descriptions = savedInstanceState.getStringArray("descriptions");
            String[] languages = savedInstanceState.getStringArray("languages");
            String[] contributors = savedInstanceState.getStringArray("contributors");
            List<RepositoryBean> savedRepos = new LinkedList<RepositoryBean>();
            if((ids!=null) && (names!=null) && (issues!=null) && (descriptions!=null) && (contributors!=null)){
                for(int i = 0; i < ids.length; i++){
                    RepositoryBean bean = new RepositoryBean(ids[i],names[i],descriptions[i],languages[i],contributors[i],issues[i]);
                    savedRepos.add(bean);
                }
            }
            if(savedRepos.size()>0){
                repositories = new LinkedList<RepositoryBean>();
                for(RepositoryBean repo : savedRepos){
                    repositories.add(repo);
                }
                setVisibilityLoading(View.GONE);
                setVisibilityToRepos(View.VISIBLE);
            }
            else{
                repositories = new LinkedList<RepositoryBean>();
            }

        }
        View view = inflater.inflate(R.layout.fragment_repositories,null);
        repositoriesList = (ListView) view.findViewById(R.id.repositories);
        loading = (ProgressBar) view.findViewById(R.id.loading);
        message = (TextView) view.findViewById(R.id.messageNotFound);
        if(repositories == null){
            repositories = new LinkedList<RepositoryBean>();
        }
        repositoryAdapter = new RepositoryAdapter(RepositoryActivity.getInstanceRepositoryActivity(),repositories);
        repositoriesList.setAdapter(repositoryAdapter);
        repositoriesList.setOnScrollListener(new EndlessScroll() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                getMoreData(page);
                return true;
            }
        });
        repositoriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showRepositoryData(repositories.get(position));
            }
        });
        return view;
    }

    @Override
    public void onViewCreated (View view, Bundle savedInstanceState) {
        repositoriesList = (ListView) view.findViewById(R.id.repositories);
        repositoryAdapter = new RepositoryAdapter(RepositoryActivity.getInstanceRepositoryActivity(),repositories);
        repositoriesList.setAdapter(repositoryAdapter);
        repositoriesList.setOnScrollListener(new EndlessScroll() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                getMoreData(page);
                return true;
            }
        });
        repositoriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showRepositoryData(repositories.get(position));
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(repositoryAdapter != null){
            String[] ids = repositoryAdapter.getIds();
            String[] names = repositoryAdapter.getNames();
            String[] issues = repositoryAdapter.getIssues();
            String[] descriptions = repositoryAdapter.getDescriptions();
            String[] languages = repositoryAdapter.getLanguages();
            String[] contributors = repositoryAdapter.getContributors();

            outState.putStringArray("ids",ids);
            outState.putStringArray("names",names);
            outState.putStringArray("issues",issues);
            outState.putStringArray("descriptions",descriptions);
            outState.putStringArray("languages",languages);
            outState.putStringArray("contributors",contributors);
            for(int i = 0; i < names.length; i++){
                Log.i("FRAGMENT_ISSUE", "save: id:"+ids[i]+",name:"+names[i]+",issues:"+issues[i]);
            }
        }
    }

    private void showRepositoryData(RepositoryBean repositoryBean) {
        ((RepositoryActivity) getActivity()).showRepositoryDetail(repositoryBean);
    }

    public void refreshAdapter(List<RepositoryBean> newRepos,String language){
        repositories = new LinkedList<>();
        for(RepositoryBean repo : newRepos){
            repositories.add(repo);
        }
        repositoryAdapter = new RepositoryAdapter(RepositoryActivity.getInstanceRepositoryActivity(),repositories);
        repositoriesList.setAdapter(repositoryAdapter);
        try{
            RepositoryActivity.getInstanceRepositoryActivity().getSupportActionBar().setTitle(language);
        }catch (Exception e){e.printStackTrace();}
    }

    public void addMoreRepositories(List<RepositoryBean> newRepositories){
        for(RepositoryBean repository : newRepositories){
            repositories.add(repository);
        }
        repositoryAdapter.notifyDataSetChanged();
    }

    public void setVisibilityLoading(int visibility){
        if( loading != null)
            loading.setVisibility(visibility);
    }

    public void setVisibilityToMessage(int visible){
        if(message != null)
            message.setVisibility(visible);
    }
    public void setVisibilityToRepos(int visible){
        if(repositoriesList != null)
            repositoriesList.setVisibility(visible);
    }

    public void setDownloadManager(DownloadManager downloadManager) {
        this.downloadManager = downloadManager;
    }

    public void clearAllData(){
        if(repositories != null){
            for(int repoCount = 0; repoCount < repositories.size(); repoCount++){
                repositories.remove(repoCount);
            }
            repositories.clear();
            if(repositoryAdapter!=null)
            repositoryAdapter.notifyDataSetChanged();
        }
    }

    public void setMessageNotFound(String language){
        if(message !=null)
            message.setText(Html.fromHtml("No se han encontrado resultados para <br>" + language + "</br>"));
        message.setVisibility(View.VISIBLE);
        try{
            RepositoryActivity.getInstanceRepositoryActivity().getSupportActionBar().setTitle("");
        }catch (Exception e){e.printStackTrace();}
    }


    private void getMoreData(int page){
        downloadManager.addMoreData(page);
    }
}
