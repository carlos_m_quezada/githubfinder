package finder.repository.github.graphics;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.List;
import finder.repository.github.bean.ContributorBean;
import finder.repository.github.bean.IssueBean;
import finder.repository.github.bean.RepositoryDetail;
import finder.repository.github.dowloadRepos.DownloadDetailManager;
import finder.repository.github.githubfinder.R;
import finder.repository.github.githubfinder.RepositoryActivity;

/**
 * Created by Carlos on 05/11/2016.
 */
public class Repository_Desc_Fragment extends Fragment {
    private static LinearLayout allDetailRepository;
    private DownloadDetailManager downloadDetailManager;
    private static TextView name;
    private TextView description;
    private TextView issues;
    private TextView contributors;
    private ScrollView scrollAllDetail;
    private LinearLayout linearLoading;
    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_desc_repository, container,false);
        allDetailRepository = (LinearLayout) view.findViewById(R.id.allDetailRepository);
        name = (TextView) view.findViewById(R.id.repoName);
        description = (TextView) view.findViewById(R.id.repoDesc);
        issues = (TextView) view.findViewById(R.id.repoIssues);
        contributors = (TextView) view.findViewById(R.id.repoContributors);
        scrollAllDetail = (ScrollView) view.findViewById(R.id.scrollAllDetail);
        linearLoading = (LinearLayout) view.findViewById(R.id.linearLoading);
        setRetainInstance(true);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        allDetailRepository = (LinearLayout) view.findViewById(R.id.allDetailRepository);
        name = (TextView) getView().findViewById(R.id.repoName);
        description = (TextView) getView().findViewById(R.id.repoDesc);
        issues = (TextView) getView().findViewById(R.id.repoIssues);
        contributors = (TextView) getView().findViewById(R.id.repoContributors);
        scrollAllDetail = (ScrollView) getView().findViewById(R.id.scrollAllDetail);
        linearLoading = (LinearLayout) getView().findViewById(R.id.linearLoading);
    }

    @Override
    public void onViewCreated (View view, Bundle savedInstanceState) {
        allDetailRepository = (LinearLayout) view.findViewById(R.id.allDetailRepository);
        name = (TextView) view.findViewById(R.id.repoName);
        description = (TextView) view.findViewById(R.id.repoDesc);
        issues = (TextView) view.findViewById(R.id.repoIssues);
        contributors = (TextView) view.findViewById(R.id.repoContributors);
        scrollAllDetail = (ScrollView) view.findViewById(R.id.scrollAllDetail);
        linearLoading = (LinearLayout) view.findViewById(R.id.linearLoading);
    }
    public void setName(String name){
        if(name!=null)
            allDetailRepository.setVisibility(View.VISIBLE);
        this.name.setText(name);
    }
    public void setDescription(String description){
        if(this.description!=null){
            allDetailRepository.setVisibility(View.VISIBLE);
            this.description.setText(description);
        }
    }

    public void setDownloadDetailManager(DownloadDetailManager downloadDetailManager) {
        this.downloadDetailManager = downloadDetailManager;
    }
    public void setVisibilityToScrollAllDetail(int visibility){
        if(this.scrollAllDetail!=null)
        this.scrollAllDetail.setVisibility(visibility);
    }
    public void setVisibilityToLoadingDetail(int visibility){
        if(linearLoading!=null)
        this.linearLoading.setVisibility(visibility);
    }
    public void showDetail(RepositoryDetail repositoryDetail){
        if((RepositoryActivity) getActivity()!=null)
        ((RepositoryActivity) getActivity()).showDetailAfterLoad(downloadDetailManager.getRepositoryBean(), repositoryDetail);
        else
        RepositoryActivity.getInstanceRepositoryActivity().showDetailAfterLoad(downloadDetailManager.getRepositoryBean(), repositoryDetail);
    }
    public void setIssues(List<IssueBean> issues){
        String issuesInLine = "";
        for(IssueBean issue : issues){
            issuesInLine += "Issue["+issue.getId()+"]\n"+issue.getTitle()+"\n"+issue.getBody()+"\n\n";
        }
        if(this.issues!=null)
            this.issues.setText(issuesInLine);
    }
    public void setContributors(List<ContributorBean> contributos){
        String contributosInLine = "";
        for(ContributorBean contributor : contributos){
            contributosInLine += contributor.getId()+"/"+contributor.getLogin()+"\nContributions: "+contributor.getContributions();
        }
        if(this.contributors != null)
        this.contributors.setText(contributosInLine);
    }
}
