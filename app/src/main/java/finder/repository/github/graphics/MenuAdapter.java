package finder.repository.github.graphics;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import finder.repository.github.bean.MenuBean;
import finder.repository.github.githubfinder.R;

/**
 * Created by Carlos on 02/11/2016.
 */
public class MenuAdapter extends ArrayAdapter{
    private Context context;
    private List<MenuBean> options;
    public MenuAdapter (Context context, List<MenuBean> options){
        super(context,0,options);
        this.context = context;
        this.options = options;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.menu_adapter, null);
        }
        ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        MenuBean item = (MenuBean) getItem(position);
        name.setTextColor(Color.WHITE);
        name.setText(item.getName());
        icon.setImageResource(item.getIcon());
        return convertView;
    }
}
