package finder.repository.github.githubfinder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.LinkedList;
import finder.repository.github.bean.ContributorBean;
import finder.repository.github.bean.IssueBean;
import finder.repository.github.bean.MenuBean;
import finder.repository.github.bean.RepositoryBean;
import finder.repository.github.bean.RepositoryDetail;
import finder.repository.github.dowloadRepos.DownloadDetailManager;
import finder.repository.github.dowloadRepos.DownloadManager;
import finder.repository.github.graphics.MenuAdapter;
import finder.repository.github.graphics.RepositoriesFragment;
import finder.repository.github.graphics.Repository_Desc_Fragment;

public class RepositoryActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {
    private final int MENU_REPOSITORIES = 0;
    private final int MENU_INFO = 1;
    private static FragmentManager fragmentManager;
    private static FragmentTransaction fragmentTransaction;
    private static RepositoriesFragment repositoriesFragment;
    private static RepositoryActivity instanceRepositoryActivity;
    private String[] optionsMenuTags;
    private DrawerLayout left_menu;
    private ListView optionsMenu;
    private ActionBarDrawerToggle left_menu_toggle;
    private DownloadManager downloadManager;
    private static Repository_Desc_Fragment detailFragment;
    DownloadDetailManager downloadDetailManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!isTablet(getApplicationContext())){
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        instanceRepositoryActivity = this;
        setContentView(R.layout.activity_repository);
        optionsMenuTags = getResources().getStringArray(R.array.menuOptions);
        left_menu = (DrawerLayout) findViewById(R.id.drawer_layout);
        left_menu_toggle = new ActionBarDrawerToggle(this,left_menu,R.drawable.ic_list_white_24dp,R.string.left_menu_open){
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(R.string.defaultLanguage);
            }
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(R.string.menu_options);
            }
        };
        left_menu.setDrawerListener(left_menu_toggle);
        try{
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }catch (Exception e){e.printStackTrace();}

        optionsMenu = (ListView) findViewById(R.id.left_menu);
        LinkedList<MenuBean> options = new LinkedList<MenuBean>();
        options.add(new MenuBean(optionsMenuTags[0], R.drawable.ic_assignment_white_24dp));
        options.add(new MenuBean((optionsMenuTags[1]), R.drawable.ic_info_outline_white_24dp));
        optionsMenu.setAdapter(new MenuAdapter(this, options));
        optionsMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                left_menu.closeDrawer(GravityCompat.START);
                setFragment(position);
            }
        });
        fragmentManager = getSupportFragmentManager();
        if(repositoriesFragment == null){
            Log.i("FRAGMENT_ISSUE","repositoriesFragmenISNULL");
            repositoriesFragment = new RepositoriesFragment();
            setFragment(MENU_REPOSITORIES);
        }
        if(detailFragment == null){
            detailFragment = new Repository_Desc_Fragment();
            setFragmentDetail(detailFragment);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        left_menu_toggle.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        left_menu_toggle.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_repository);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.search)
                .getActionView();
        if (null != searchView) {
            searchView.setSearchableInfo(searchManager
                    .getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(false);
        }

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                //alert(newText+" OnQueryTextListener");
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                if(query !=null && query.length()>0){
                    downloadManager = new DownloadManager(getInstanceRepositoryActivity(),repositoriesFragment);
                    downloadManager.setLanguage(query.replace(" ",""));
                    downloadManager.setPage(0);
                    downloadManager.newQuery();
                }
                return  true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

        }
        if (left_menu_toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }
    private void setFragment(int position){
        switch (position){
            case MENU_REPOSITORIES :
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content,repositoriesFragment);
                fragmentTransaction.commit();
                downloadManager = new DownloadManager(getInstanceRepositoryActivity(),repositoriesFragment);
                downloadManager.setLanguage("Java");
                downloadManager.setPage(0);
                downloadManager.newQuery();
                break;
            case MENU_INFO :
                alert(getResources().getString(R.string.carlos));
                break;
        }
    }

    public void setFragmentDetail(Fragment frag) {
        FragmentManager fm = getSupportFragmentManager();
        if(isTablet(getApplicationContext())){
            if (fm.findFragmentById(R.id.contentDetail) == null) {
                fm.beginTransaction().add(R.id.contentDetail, frag).commit();
            }
        }
    }
    public static void alert(String message){
        AlertDialog alertDialog = new AlertDialog.Builder(getInstanceRepositoryActivity()).create();
        alertDialog.setTitle(R.string.app_name);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public static RepositoryActivity getInstanceRepositoryActivity() {
        return instanceRepositoryActivity;
    }

    public static boolean isTablet(Context context){
        return  (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public void showRepositoryDetail(RepositoryBean repositoryBean){
        downloadDetailManager = new DownloadDetailManager(this,detailFragment);
        downloadDetailManager.setRepositoryBean(repositoryBean);
        downloadDetailManager.goForDetail();
    }

    private static Dialog fillDialog(RepositoryBean repositoryBean,RepositoryDetail repositoryDetail){
        Dialog detail = new Dialog(getInstanceRepositoryActivity(),android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        detail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        detail.setContentView(R.layout.fragment_desc_repository);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(detail.getWindow().getAttributes());
        detail.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        ((TextView) detail.findViewById(R.id.repoName)).setText(repositoryBean.getName());
        ((TextView) detail.findViewById(R.id.repoDesc)).setText(repositoryBean.getDescription());
        String issuesInLine = "";
        if(repositoryDetail != null){
            if(repositoryDetail.getIssues() != null){
                for(IssueBean issue : repositoryDetail.getIssues()){
                    issuesInLine += "Issue["+issue.getId()+"]\n"+issue.getTitle()+"\n"+issue.getBody()+"\n\n";
                }
            }
            ((TextView) detail.findViewById(R.id.repoIssues)).setText(issuesInLine);
            String contributosInLine = "";
            if(repositoryDetail.getContributors() != null){
                for(ContributorBean contributor : repositoryDetail.getContributors()){
                    contributosInLine += contributor.getId()+"/"+contributor.getLogin()+"\nContributions: "+contributor.getContributions()+"\n\n";
                }
            }
            ((TextView) detail.findViewById(R.id.repoContributors)).setText(contributosInLine);
        }

        ((ScrollView) detail.findViewById(R.id.scrollAllDetail)).setVisibility(View.VISIBLE);

        return detail;
    }

    public void showDetailAfterLoad(RepositoryBean repositoryBean,RepositoryDetail repositoryDetail){
        if(isTablet(getInstanceRepositoryActivity())){
            FrameLayout detail = (FrameLayout) getInstanceRepositoryActivity().findViewById(R.id.contentDetail);
            if(detail != null){
                fragmentManager = getInstanceRepositoryActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                detailFragment.setName(repositoryBean.getName());
                detailFragment.setDescription(repositoryBean.getDescription());
                detailFragment.setIssues(repositoryDetail.getIssues());
                detailFragment.setContributors(repositoryDetail.getContributors());
                setFragmentDetail(detailFragment);
            }
        }
        else{
            fillDialog(repositoryBean,repositoryDetail).show();
        }
    }
}
