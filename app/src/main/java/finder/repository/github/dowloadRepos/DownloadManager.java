package finder.repository.github.dowloadRepos;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.util.Log;
import finder.repository.github.graphics.RepositoriesFragment;

/**
 * Created by Carlos on 04/11/2016.
 */
public class DownloadManager {
    private Activity activity;
    private DownLoadRepositories downLoadRepositories;
    private Fragment fragment;
    private int page;
    private String language;

    public DownloadManager(Activity activity,Fragment fragment){
        this.activity = activity;
        this.fragment = fragment;
        this.downLoadRepositories = new DownLoadRepositories(activity);
        this.downLoadRepositories.setPagination(1);
        this.downLoadRepositories.setFragment(fragment);
        if(fragment instanceof RepositoriesFragment){
            ((RepositoriesFragment)fragment).setDownloadManager(this);
        }
    }

    public void newQuery(){
        if(fragment instanceof RepositoriesFragment){
            ((RepositoriesFragment)fragment).clearAllData();
            downLoadRepositories.setLanguage(language);
            downLoadRepositories.execute(DownLoadRepositories.NEW_QUERY);
        }
        else if(fragment == null){
            Log.i("newMethod","El fragment es NULL");
        }
    }

    public void addMoreData(int page){
        this.downLoadRepositories = new DownLoadRepositories(activity);
        this.downLoadRepositories.setPagination(1);
        this.downLoadRepositories.setFragment(fragment);
        downLoadRepositories.setLanguage(language);
        downLoadRepositories.setPagination(page);
        downLoadRepositories.execute(DownLoadRepositories.GET_MORE_DATA);
    }

    public void setPage(int page) {
        this.page = page;
    }
    public void setLanguage(String language) {
        this.language = language;
    }
    public void setFragment(Fragment fragment){
        this.fragment = fragment;
    }
}
