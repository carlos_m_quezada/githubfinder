package finder.repository.github.dowloadRepos;

import android.app.Activity;
import finder.repository.github.bean.RepositoryBean;
import finder.repository.github.graphics.Repository_Desc_Fragment;

/**
 * Created by Carlos on 06/11/2016.
 */
public class DownloadDetailManager {
    private Activity activity;
    private DownloadDetail downloadDetail;
    private Repository_Desc_Fragment fragment;
    private RepositoryBean repositoryBean;

    public DownloadDetailManager(Activity activity,Repository_Desc_Fragment fragment){
        this.activity = activity;
        this.fragment = fragment;
        if(fragment instanceof Repository_Desc_Fragment){
            ((Repository_Desc_Fragment)fragment).setDownloadDetailManager(this);
        }
    }

    public void goForDetail(){
        downloadDetail = new DownloadDetail();
        downloadDetail.setFragment(fragment);
        downloadDetail.execute(repositoryBean.getIssues_url(),repositoryBean.getContributors_link());
    }

    public void setRepositoryBean(RepositoryBean repositoryBean) {
        this.repositoryBean = repositoryBean;
    }

    public Repository_Desc_Fragment getFragment() {
        return fragment;
    }

    public RepositoryBean getRepositoryBean() {
        return repositoryBean;
    }
}
