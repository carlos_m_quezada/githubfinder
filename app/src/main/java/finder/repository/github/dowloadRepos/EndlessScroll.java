package finder.repository.github.dowloadRepos;

import android.util.Log;
import android.widget.AbsListView;

/**
 * Created by Carlos on 03/11/2016.
 */
public abstract class EndlessScroll implements AbsListView.OnScrollListener {
    private int visibleThreshold = 5;
    private int currentPage = 0;
    private int previousTotalItemCount = 0;
    private boolean loading = true;
    private int startingPageIndex = 0;
    public EndlessScroll() {
    }
    public EndlessScroll(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }
    public EndlessScroll(int visibleThreshold, int startPage) {
        this.visibleThreshold = visibleThreshold;
        this.startingPageIndex = startPage;
        this.currentPage = startPage;
    }
    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }
    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) { this.loading = true; }
        }
        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
            currentPage++;
        }

        if (!loading && (firstVisibleItem + visibleItemCount + visibleThreshold) >= totalItemCount ) {
            Log.i("ENDLESSCROLL","loading="+loading+" firstVisibleItem:"+firstVisibleItem+" visibleItemCount:"+visibleItemCount+" visibleThreshold:"+visibleThreshold+" totalItemCount:"+totalItemCount);
            loading = onLoadMore(currentPage + 1, totalItemCount);
        }
    }

    public abstract boolean onLoadMore(int page, int totalItemsCount);
}
