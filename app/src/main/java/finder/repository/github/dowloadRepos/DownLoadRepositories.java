package finder.repository.github.dowloadRepos;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import finder.repository.github.bean.HeaderGitHub;
import finder.repository.github.bean.RepositoryBean;
import finder.repository.github.githubfinder.RepositoryActivity;
import finder.repository.github.graphics.RepositoriesFragment;

/**
 * Created by Carlos on 02/11/2016.
 */
public class DownLoadRepositories extends AsyncTask<String,Integer,List<RepositoryBean>> {
    private String mode;
    private String language;
    private int pagination = 1;
    private final int per_page=15;
    private RepositoriesFragment repositoriesFragment;
    public final String TAG = "TAG_DOWNLOAD";
    public static final String GET_ALL = "GET_ALL";
    public static final String NEW_QUERY = "NEW_QUERY";
    public static final String GET_MORE_DATA = "GET_MORE_DATA";
    private Context context;
    public DownLoadRepositories(Context context){
        this.context = context;
    }
    private List<RepositoryBean> newRepositories;
    private String url_all = "https://api.github.com/repositories";
    private HttpClient httpClient;
    private HttpGet httpGet;

    @Override
    protected void onPreExecute() {
        repositoriesFragment.setVisibilityToMessage(View.GONE);
        repositoriesFragment.setVisibilityLoading(View.VISIBLE);
    }

    @Override
    protected List doInBackground(String... params) {
        if(params!=null && params[0]!=null){
            mode = params[0];
            switch (mode){
                case NEW_QUERY: newRepositories = getRepositories(pagination, language);
                    break;
                case GET_MORE_DATA:
                    newRepositories = getRepositories(pagination,language);
                    break;
            }
        }
        return newRepositories;
    }

    @Override
    protected void onPostExecute(List<RepositoryBean> reposPostExecute) {

        Log.i(TAG, "reposPostExecute.size()="+reposPostExecute.size());
        if(reposPostExecute!=null && reposPostExecute.size() > 0){
            repositoriesFragment.setVisibilityToRepos(View.VISIBLE);
            if(pagination == 1){
                repositoriesFragment.refreshAdapter(reposPostExecute,language);
            }
            else{
                repositoriesFragment.addMoreRepositories(reposPostExecute);
            }
        }
        else{
            if(pagination==1){
                repositoriesFragment.setVisibilityToRepos(View.GONE);
                repositoriesFragment.setMessageNotFound(language);
                repositoriesFragment.setVisibilityToMessage(View.VISIBLE);
            }
            else{
                RepositoryActivity.alert("No se encontraron más resultados para "+language);
            }
        }
        repositoriesFragment.setVisibilityLoading(View.GONE);
    }

    private String getHttpClientByMode(String mode,int page, String language){
        switch (mode){
            case GET_ALL: return "https://api.github.com/repositories";
            case NEW_QUERY: return  "https://api.github.com/search/repositories?q=language:"+language+"&per_page="+per_page+"&page="+page;
            case GET_MORE_DATA: return  "https://api.github.com/search/repositories?q=language:"+language+"&per_page="+per_page+"&page="+page;
        }
        return null;
    }

    private HeaderGitHub getHeadersForResponse(HttpResponse response){
        Header[] headers = response.getAllHeaders();
        HeaderGitHub headerGitHub = null;
        if(headers != null){
            headerGitHub = new HeaderGitHub();
            for(int line = 0; line < headers.length; line++){
                if(headers[line].getName().equalsIgnoreCase("server")){ headerGitHub.setServer(headers[line].getValue());}
                if(headers[line].getName().equalsIgnoreCase("date")){ headerGitHub.setDate(headers[line].getValue());
                }
                if(headers[line].getName().equalsIgnoreCase("Content-Type")){ headerGitHub.setContent_type(headers[line].getValue());
                }
                if(headers[line].getName().equalsIgnoreCase("Content-Length")){ headerGitHub.setContent_length(headers[line].getValue());
                }
                if(headers[line].getName().equalsIgnoreCase("status")){ headerGitHub.setStatus(headers[line].getValue());
                }
                if(headers[line].getName().equalsIgnoreCase("X-RateLimit-Limit")){ headerGitHub.setX_rateLimit_limit(headers[line].getValue());}
                if(headers[line].getName().equalsIgnoreCase("X-RateLimit-Remaining")){ headerGitHub.setX_rateLimit_remaining(headers[line].getValue());}
                if(headers[line].getName().equalsIgnoreCase("X-RateLimit-Reset")){ headerGitHub.setX_rateLimit_reset(headers[line].getValue());}
                if(headers[line].getName().equalsIgnoreCase("Link")){ headerGitHub.setLink(headers[line].getValue());}
            }
        }
        return  headerGitHub;
    }
    private HttpGet getClientForUrl(String url){
        HttpGet get = new HttpGet(url);
        get.setHeader("content-type","application/json");
        get.setHeader("User-Agent","Android");
        return get;
    }
    private List<RepositoryBean> getRepositories(int pagination){
        newRepositories = new LinkedList<RepositoryBean>();
        httpClient = new DefaultHttpClient();
        httpGet = getClientForUrl(url_all);
        try {
            HttpResponse response = httpClient.execute(httpGet);
            HeaderGitHub headerGitHub = getHeadersForResponse(response);
            Log.i("HEADERS_NEW",headerGitHub.toString());
            String reponseInText = EntityUtils.toString(response.getEntity());
            JSONArray all_repositories = new JSONArray(reponseInText);
            Log.i(TAG,"La respuesta contiene "+all_repositories.length()+"elementos: "+reponseInText);
            if (all_repositories != null && all_repositories.length() > 0) {
                for(int count = 0; count < all_repositories.length(); count++){
                    JSONObject repository  = all_repositories.getJSONObject(count);
                    String id = repository.getString("id");
                    String name = repository.getString("full_name");
                    String description = repository.getString("description");
                    String language_link = repository.getString("languages_url");
                    String issues_links = repository.getString("issues_url");
                    String contributors_url = repository.getString("contributors_url");
                    RepositoryBean repositoryBean = new RepositoryBean(name,description);
                    repositoryBean.setId(id);
                    repositoryBean.setLanguage_link(language_link);
                    repositoryBean.setIssues_url(issues_links);
                    repositoryBean.setContributors_link(contributors_url);
                    newRepositories.add(repositoryBean);
                }
            }
            else{
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return newRepositories;
    }

    private List<RepositoryBean> getRepositories(int pagination,String language){
            newRepositories = new LinkedList<RepositoryBean>();
        httpClient = new DefaultHttpClient();
        String url = getHttpClientByMode(NEW_QUERY,pagination,language);
        httpGet = getClientForUrl(url);
        try {
            Log.i(TAG, "Se comenzará la descarga con este link: "+url);
            HttpResponse response = httpClient.execute(httpGet);
            HeaderGitHub headerGitHub = getHeadersForResponse(response);
            Log.i("HEADERS_NEW",headerGitHub.toString());
            String reponseInText = EntityUtils.toString(response.getEntity());
            JSONObject all_repositories = new JSONObject(reponseInText);
            if (all_repositories != null && all_repositories.length() > 0) {
                    if(all_repositories != null){
                        JSONArray repositories = all_repositories.getJSONArray("items");
                        if(repositories != null && repositories.length() > 0){
                            Log.i(TAG,"La respuesta contiene "+repositories.length()+" repositorios: "+reponseInText);
                            for(int item = 0; item < repositories.length(); item++){
                                JSONObject repository = repositories.getJSONObject(item);
                                String id = repository.getString("id");
                                String name = repository.getString("full_name");
                                String description = repository.getString("description");
                                String language_link = repository.getString("languages_url");
                                String issues_links = repository.getString("issues_url");
                                String contributors_url = repository.getString("contributors_url");
                                RepositoryBean repositoryBean = new RepositoryBean(name,description);
                                repositoryBean.setId(id);
                                repositoryBean.setLanguage_link(language_link);
                                repositoryBean.setIssues_url(issues_links);
                                repositoryBean.setContributors_link(contributors_url);
                                newRepositories.add(repositoryBean);
                            }
                        }
                    }
            }
            else{
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return newRepositories;
    }

    private List<RepositoryBean> getallRepositoyiesByLanguage(int page,String language){
        return getRepositories(page,language);
    }

    private List<RepositoryBean> getAllRepositories(int page){
        return getRepositories(page);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setPagination(int pagination) {
        this.pagination = pagination;
    }
    public void setFragment(Fragment fragment){
        if(fragment instanceof RepositoriesFragment){
            this.repositoriesFragment = (RepositoriesFragment)fragment;
        }
    }
}
