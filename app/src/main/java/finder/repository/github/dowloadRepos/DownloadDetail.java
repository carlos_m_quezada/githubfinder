package finder.repository.github.dowloadRepos;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import finder.repository.github.bean.ContributorBean;
import finder.repository.github.bean.IssueBean;
import finder.repository.github.bean.RepositoryDetail;
import finder.repository.github.graphics.Repository_Desc_Fragment;

/**
 * Created by Carlos on 06/11/2016.
 */
public class DownloadDetail extends AsyncTask<String,Integer,RepositoryDetail> {

    private RepositoryDetail detail;
    private List<IssueBean> issues;
    private List<ContributorBean> contributors;
    private String issues_link;
    private String contributors_link;
    private HttpClient httpClient;
    private HttpGet httpGet;
    private Repository_Desc_Fragment fragment;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        fragment.setVisibilityToLoadingDetail(View.VISIBLE);
    }

    @Override
    protected RepositoryDetail doInBackground(String... params) {
        if(params!=null && params.length ==2){
            detail = new RepositoryDetail();
            issues_link = params[0]+"/issues?per_page=3";
            contributors_link = params[1]+"?per_page=3";
            issues = getIssues(issues_link);
            contributors = getContributors(contributors_link);
            detail = new RepositoryDetail();
            detail.setIssues(issues);
            detail.setContributors(contributors);
        }
        return detail;
    }

    @Override
    protected void onPostExecute(RepositoryDetail repositoryDetail) {
        super.onPostExecute(repositoryDetail);
        fragment.setVisibilityToLoadingDetail(View.GONE);
        fragment.setVisibilityToScrollAllDetail(View.VISIBLE);
        fragment.showDetail(repositoryDetail);
    }

    private HttpGet getClientForUrl(String url){
        HttpGet get = new HttpGet(url);
        get.setHeader("content-type","application/json");
        get.setHeader("User-Agent","Android");
        return get;
    }

    private List<IssueBean> getIssues(String url_issues){
        Log.i("DETAIL","Issues link= "+url_issues);
        issues = new LinkedList<IssueBean>();
        httpClient = new DefaultHttpClient();
        httpGet = getClientForUrl(url_issues);
        try {
            HttpResponse response = httpClient.execute(httpGet);
            String reponseInText = EntityUtils.toString(response.getEntity());
            Log.i("DETAIL","Response Issues= "+reponseInText);
            JSONArray all_repositories = new JSONArray(reponseInText);
            if (all_repositories != null && all_repositories.length() > 0) {
                for(int count = 0; count < all_repositories.length(); count++){
                    JSONObject repository  = all_repositories.getJSONObject(count);
                    String id = repository.getString("id");
                    String title = repository.getString("title");
                    String body = repository.getString("body");
                    IssueBean issue = new IssueBean(id,title,body);
                    issues.add(issue);
                }
            }
            else{
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return issues;
    }

    private List<ContributorBean> getContributors(String url_contributors){
        Log.i("DETAIL","Issues link= "+url_contributors);
        contributors = new LinkedList<ContributorBean>();
        httpClient = new DefaultHttpClient();
        httpGet = getClientForUrl(url_contributors);
        try {
            HttpResponse response = httpClient.execute(httpGet);
            String reponseInText = EntityUtils.toString(response.getEntity());
            Log.i("DETAIL","Response Contributors= "+reponseInText);
            JSONArray all_repositories = new JSONArray(reponseInText);
            if (all_repositories != null && all_repositories.length() > 0) {
                for(int count = 0; count < all_repositories.length(); count++){
                    JSONObject repository  = all_repositories.getJSONObject(count);
                    String id = repository.getString("id");
                    String login = repository.getString("login");
                    String contributions = repository.getString("contributions");
                    ContributorBean contributor = new ContributorBean(id,login,contributions);
                    contributors.add(contributor);
                }
            }
            else{
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return contributors;
    }

    public Repository_Desc_Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Repository_Desc_Fragment fragment) {
        this.fragment = fragment;
    }
}
