package finder.repository.github.bean;

/**
 * Created by Carlos on 02/11/2016.
 */
public class RepositoryBean{
    private String id;
    private String name;
    private String description="";
    private String language_link;
    private String contributors_link;
    private String issues_url;

    public RepositoryBean(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public RepositoryBean(String id, String name, String description, String language_link, String contributors_link, String issues_url) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.language_link = language_link;
        this.contributors_link = contributors_link;
        this.issues_url = issues_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage_link() {
        return language_link;
    }

    public void setLanguage_link(String language_link) {
        this.language_link = language_link;
    }

    public String getContributors_link() {
        return contributors_link;
    }

    public void setContributors_link(String contributors_link) {
        this.contributors_link = contributors_link;
    }

    public String getIssues_url() {
        if(issues_url.contains("/issues{/number}"))
        return issues_url.replace("/issues{/number}","");
        else return issues_url;
    }

    public void setIssues_url(String issues_url) {
        this.issues_url = issues_url;
    }

    @Override
    public String toString() {
        return "RepositoryBean{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", language_link='" + language_link + '\'' +
                ", contributors_link='" + contributors_link + '\'' +
                ", issues_url='" + issues_url + '\'' +
                '}';
    }
}

