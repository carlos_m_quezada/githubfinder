package finder.repository.github.bean;

/**
 * Created by Carlos on 03/11/2016.
 */
public class HeaderGitHub {
    private String server;
    private String date;
    private String content_type;
    private String content_length;
    private String status;
    private String x_rateLimit_limit;
    private String x_rateLimit_remaining;
    private String x_rateLimit_reset;
    private String link;
    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getContent_length() {
        return content_length;
    }

    public void setContent_length(String content_length) {
        this.content_length = content_length;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getX_rateLimit_limit() {
        return x_rateLimit_limit;
    }

    public void setX_rateLimit_limit(String x_rateLimit_limit) {
        this.x_rateLimit_limit = x_rateLimit_limit;
    }

    public String getX_rateLimit_remaining() {
        return x_rateLimit_remaining;
    }

    public void setX_rateLimit_remaining(String x_rateLimit_remaining) {
        this.x_rateLimit_remaining = x_rateLimit_remaining;
    }

    public String getX_rateLimit_reset() {
        return x_rateLimit_reset;
    }

    public void setX_rateLimit_reset(String x_rateLimit_reset) {
        this.x_rateLimit_reset = x_rateLimit_reset;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "HeaderGitHub{" +
                "server='" + server + '\'' +
                ", date='" + date + '\'' +
                ", content_type='" + content_type + '\'' +
                ", content_length='" + content_length + '\'' +
                ", status='" + status + '\'' +
                ", x_rateLimit_limit='" + x_rateLimit_limit + '\'' +
                ", x_rateLimit_remaining='" + x_rateLimit_remaining + '\'' +
                ", x_rateLimit_reset='" + x_rateLimit_reset + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
