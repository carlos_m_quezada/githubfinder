package finder.repository.github.bean;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Carlos on 06/11/2016.
 */
public class RepositoryDetail {
    private List<IssueBean> issues = new LinkedList<>();
    private List<ContributorBean> contributors = new LinkedList<>();

    public List<IssueBean> getIssues() {
        return issues;
    }

    public void setIssues(List<IssueBean> issues) {
        this.issues = issues;
    }

    public List<ContributorBean> getContributors() {
        return contributors;
    }

    public void setContributors(List<ContributorBean> contributors) {
        this.contributors = contributors;
    }
}
