package finder.repository.github.bean;

/**
 * Created by Carlos on 02/11/2016.
 */
public class MenuBean {
    private String name;
    private int icon;
    public MenuBean(String name, int icon){
        this.name = name;
        this.icon = icon;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
