package finder.repository.github.bean;

/**
 * Created by Carlos on 07/11/2016 and modified by me only for github test
 */
public class ContributorBean {

    private String id;
    private String login;
    private String contributions;

    public ContributorBean(String id, String login, String contributions) {
        this.id = id;
        this.login = login;
        this.contributions = contributions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getContributions() {
        return contributions;
    }

    public void setContributions(String contributions) {
        this.contributions = contributions;
    }
}
